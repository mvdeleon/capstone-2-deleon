const express = require("express");
const router = express.Router();


const ordersController = require("../controllers/ordersController");

const auth = require("../auth");


// 1. CHECK ALL ORDERS (OWN ORDERS)

router.get("/checkOrder",auth.verify,(req,res)=>{

	let data = {
		userId : auth.decode(req.headers.authorization).id,

		}

	ordersController.allOrders(data).then(resultFromController=>res.send(resultFromController));
});




// 2. CHECK A SINGLE ORDER (OWN ORDER)

router.get("/checkOrder/:orderId",auth.verify,(req,res)=>{

	let data = {
		userId : auth.decode(req.headers.authorization).id,

		}

	ordersController.getOrder(data,req.params,req.body).then(resultFromController=>res.send(resultFromController));
});



// 3. CHANGE ORDER QUANTITY (OWN ORDER)

router.put("/:orderId",auth.verify,(req,res)=>{

	const userId = auth.decode(req.headers.authorization);


	ordersController.updateOrder(req.params,req.body).then(resultFromController=>res.send(resultFromController));


});



// 4. REMOVE PRODUCTS FROM CART (OWN ORDER)


router.delete("/remove/:orderId",auth.verify,(req,res)=>{

	let data = {
		userId : auth.decode(req.headers.authorization).id,

		}

	ordersController.removeOrder(data,req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


// 5. CHECK OUT ORDER (OWN ORDER)



router.post("/checkout/:orderId",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,

		

	}

	ordersController.createOrder(data,req.params,req.body).then(resultFromController=>res.send(resultFromController))
})



















module.exports = router;
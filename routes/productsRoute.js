const express = require("express");
const router = express.Router();


const productsController = require("../controllers/productsController");
const auth = require("../auth");


// 1. CREATE PRODUCT INFO

router.post("/create",auth.verify,(req,res)=>{

	const data={
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productsController.addProduct(data)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});
	
// 2. RETRIEVE ALL PRODUCTS

router.get("/all",(req,res)=>{

	productsController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});



// 3. RETRIEVE ALL ACTIVE PRODUCTS

router.get("/active",(req,res)=>{
	productsController.getAllActive().then(resultFromController=>res.send(resultFromController));
});


// PULL FEATURED

router.get("/featured",(req,res)=>{
	productsController.getAllFeatured().then(resultFromController=>res.send(resultFromController));
});


// 4. SEARCH FOR SPECIFIC PRODUCT

router.get("/:productId",(req,res)=>{
	productsController.getProduct(req.params).then(resultFromController=>res.send(resultFromController));
});



// 5. UPDATING A PRODUCT INFO (ADMIN ONLY)

router.put("/:productId",auth.verify,(req,res)=>{

	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productsController.updateProduct(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});



// 6. ARCHIVING A PRODUCT (ADMIN ONLY)


router.put("/archive/:productId",auth.verify,(req,res)=>{

	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productsController.archiveProduct(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});


// 7. ACTIVATING A PRODUCT (ADMIN ONLY)


router.put("/activate/:productId",auth.verify,(req,res)=>{

	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productsController.activateProduct(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});

// FEATURE


router.put("/feature/:productId",auth.verify,(req,res)=>{

	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productsController.featureProduct(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});


// Remove feature


router.put("/unfeature/:productId",auth.verify,(req,res)=>{

	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productsController.unfeature(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});

// 8. GET ALL ORDERS PER PRODUCT ID (ADMIN ONLY)



router.get("/orders/:productId",auth.verify,(req,res)=>{

	const data={
		
	isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productsController.getOrders(data,req.params)
	.then(result=>res.send(result))
	.catch(error => res.send(error));
});

// 9. ADD PRODUCT FEEDBACK (Any Auth user)

router.post("/feedback/:productId",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		comment: req.body.comment	

	}

	productsController.productFeedback(data,req.params,req.body).then(resultFromController=>res.send(resultFromController))
})


// 10. Retrieve all Product Feedback

router.get("/showFeedback/:productId",(req,res)=>{

	productsController.getfeedback(req.params)
	.then(result=>res.send(result))
	.catch(error => res.send(error));
});




// 10. Retrieve all Product Feedback

router.get("/group/:category",(req,res)=>{


	

	productsController.getCat(req.params)
	.then(result=>res.send(result))
	.catch(error => res.send(error));
});





















module.exports = router;
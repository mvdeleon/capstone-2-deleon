const express = require("express");
const router = express.Router();


const usersController = require("../controllers/usersController");

const auth = require("../auth");



// 1. CHECK IF ACCOUNT EXISTS

router.post("/check",(req,res)=>{
	usersController.checkAccountExists(req.body).then(resultFromController=>res.send(resultFromController));
});

// 2. SIGN UP

router.post("/signup",(req,res)=>{
	usersController.signUpUser(req.body).then(resultFromController=>res.send(resultFromController));
});


// EDIT USER INFO

router.put("/updateUser",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		
	}

	usersController.updateUser(data,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});



// 3. USER VERIFICATION/AUTHENTICATION

router.post("/login",(req,res)=>{
	usersController.verifyUser(req.body).then(resultFromController=>res.send(resultFromController))
});





// 4. RETRIEVE OWN INFO (Any Authenticated User)- (TOKEN ONLY)

router.get("/info",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	usersController.getUserInfo({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});




// 5. RETRIEVE ALL USERS'S INFO (ADMIN ONLY)

router.get("/all",auth.verify,(req,res)=>{
 
	const data={
		
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		
	};

	usersController.getAllUsers(data)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});



// 6. RETRIEVE SPECIFIC USER'S INFO USING EMAIL (ADMIN ONLY)

router.get("/checkUser",auth.verify,(req,res)=>{
 
	const data={

		email: req.body.email,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
		
	}
          
	usersController.retrieveUserData(data)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});


// 7. CHANGE USER TO ADMIN (ADMIN ONLY)

router.put("/activateAdmin/:userId",auth.verify,(req,res)=>{

	const data={

		isAdmin: auth.decode(req.headers.authorization).isAdmin
		
	}

	usersController.activateAdmin(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});

// 8. CHANGE ADMIN TO USER  (ADMIN ONLY)

router.put("/deactivateAdmin/:userId",auth.verify,(req,res)=>{

	const data={

		isAdmin: auth.decode(req.headers.authorization).isAdmin
		
	}

	usersController.dectivateAdmin(data,req.params,req.body)
	.then(result=>res.send(result))
	.catch(error => res.send(error));


});


// 9. CREATE ORDER

router.post("/createOrder",auth.verify,(req,res)=>{

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		quantity: req.body.quantity,
		productId: req.body.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
		

	}

	usersController.createOrder(data)
	.then(result=>res.send(result))
	.catch(error => res.send(error));
})





// 10. RETRIEVE OWN ORDERS (Any Authenticated User)- (TOKEN ONLY)

router.get("/showCart",auth.verify,(req,res)=>{



	const userData = auth.decode(req.headers.authorization);

	usersController.showCart({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
});



// XXXXXXXXXXXXXXXXXXXXXXXX

// 11. Change Email

router.put("/changePassword",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);


usersController.changePassword(req.body,{userId:userData.id}).then(resultFromController=>res.send(resultFromController));


});







module.exports = router;
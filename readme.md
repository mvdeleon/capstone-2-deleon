

Project Name: The Lion's Tequilla Shop

Regular User Credentials:
email: "user@mail.com"
password: "user1234"


{
    "firstName": "Amy Farrah",
    "lastName": "Fowler",
    "email": "user@mail.com",
    "mobileNumber": "09188877799",
    "password":"user1234"

}

ID: 636bfe1ab3276a1a38b226f7

Token:

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNmJmZTFhYjMyNzZhMWEzOGIyMjZmNyIsImVtYWlsIjoidXNlckBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NjgwMjE4MDZ9.H0fjIexVfePnDkCcavfS29Cy-S68z16oPEou4k-6Auw

-------------------------------
Admin Credentials:
email: "admin@mail.com"
password: "admin1234"

{
    "firstName": "Sheldon",
    "lastName": "Cooper",
    "email": "admin@mail.com",
    "mobileNumber": "09188877733",
    "password":"admin1234"

}
ID: 636bfde2b3276a1a38b226f5


Token:

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYzNmJmZGUyYjMyNzZhMWEzOGIyMjZmNSIsImVtYWlsIjoiYWRtaW5AbWFpbC5jb20iLCJpc0FkbWluIjp0cnVlLCJpYXQiOjE2NjgwMjE4Mjd9.esb7HeuxbZpD5yNJWxKxe3CzdqEdC7t0_8nfJHi-Bcc


---------------------------------

Features:
User registration - Done
User authentication - Done
Set user as admin (Admin only) - Done (incl admin - user)
Retrieve all active products - Done
Retrieve single product - Done
Retrieve all products - Done
Create Product (Admin only) - Done
Update Product information (Admin only) - Done
Archive Product (Admin only) - Done
Make Product Available (Admin only) - Done

Non-admin User checkout (Create Order) - Done
Retrieve authenticated user’s orders - Done
Retrieve all orders (Admin only) - Done
Retrieve User Details - Done

Other Features:
Add to Cart
-Added Products - Done
-Change product quantities - Done
-Remove products from cart - Done
-Subtotal for each item - Done
-Total price for all items


Pushing Instructions

Session 1

	-Create a new repo called capstone-2-<lastName>
	-initialize capstone2-<lastName> folder as a local repo: git init
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "Includes Session 1 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	Link your capstone-2-<lastName> repo to boodle:
	WDC028v1.5b-42 | Capstone 2 Development - Ecommerce API

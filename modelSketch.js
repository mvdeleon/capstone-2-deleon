/*

App: Booking System API

Online Shop System (Web App)


-Member login
-Member registration

Customer/authenticated Users:
-View all active products
-Add to Cart and save
-Check total
-Check Out (payment: COD)

Seller users:
-add products
-update products
-archive/unarchive a product (availability)
-view all products
-view/manage user accounts**
-view all purchases

all users(guests, customers, admin)
-view active products
-send inquiry


Data Model for the booking system


/* WITH REFERENCING

members {
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	purchases:[{

		id - document identifier
		productId - the unique identifier
		productName
		quantity
		isPaid,
		datePurchased
		

	}]
}

products {
	
	id - unique for the document
	name,
	description,
	price,
	quantity,
	isAvailable,
	createdOn,
	feedback (auth and verified buyer)

}

purchases (admin only) [


		productId - the unique identifier
		productName:
		totalPurchases: (quantity)
		totalPayment: (total sold)
		customerNames: [
			userId:
			users:
			quantity:
		]
	}

*/

/*




*/

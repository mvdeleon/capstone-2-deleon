const Products = require("../models/Products");
const Users = require("../models/Users");
const auth = require("../auth");




// 1. CREATE PRODUCT INFO (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.addProduct = (data)=>{

	if(data.isAdmin){

		let newProducts = new Products({
			name: data.products.name,
			description: data.products.description,
			price: data.products.price,
			category: data.products.category,
			photo: data.products.photo
			
		})

		return newProducts.save().then((products,error)=>{
			if(error){
				return false;
			}else{
				return ("New Product Save!");
			}
		});
	}else{
		return Promise.reject('Not authorized to access this page');
	}

};

// 2. RETRIEVE ALL PRODUCTS

module.exports.getAllProducts = () => {
	return Products.find({}).then(result=>{
		return result;
	});
};


// 3. RETRIEVE ALL ACTIVE PRODUCTS

module.exports.getAllActive = () => {
	return Products.find({isActive:true}).then(result=>{
		return result;
	})
}

// PULL FEATURED

module.exports.getAllFeatured = () => {
	return Products.find({isFeatured:true}).then(result=>{
		return result;
	})
}




// 4. SEARCH FOR SPECIFIC PRODUCT
// 

module.exports.getProduct = (reqParams) =>{
	return Products.findById(reqParams.productId).then(result=>{
		return result;
	});
};


// 5. UPDATING A PRODUCT INFO (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.updateProduct = (data,reqParams,reqBody) =>{

if(data.isAdmin){

	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		category: reqBody.category,
		photo: reqBody.photo
		
	}
	return Products.findByIdAndUpdate(reqParams.productId,updatedProduct).then((products,error)=>{
		if(error){
			return false;
		}else{
			return (`Product detail/s has been updated.`);
		};
	
		});

	}else{
		return Promise.reject('Not authorized to access this page');
	};
};

// 6. ARCHIVING A PRODUCT (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.archiveProduct = (data,reqParams,reqBody) =>{

	if(data.isAdmin){
	let archivedProduct = {
		isActive: false

	}
	return Products.findByIdAndUpdate(reqParams.productId,archivedProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return (`You have successfully archived product ID: ${reqParams.productId}`);
		};
	
	});
	}else{
		return Promise.reject('Not authorized to access this page');
	};
};

// 7. ACTIVATING A PRODUCT (ADMIN ONLY)

module.exports.activateProduct = (data,reqParams,reqBody) =>{

	if(data.isAdmin){
	let activatedProduct = {
		isActive: true

	}
	return Products.findByIdAndUpdate(reqParams.productId,activatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return (`You have successfully activated product ID: ${reqParams.productId}`);;
			};
		
		});
	}else{
		return Promise.reject('Not authorized to access this page');
	}
};

// FEATURE

module.exports.featureProduct = (data,reqParams,reqBody) =>{

	if(data.isAdmin){
	let activatedProduct = {
		isFeatured: true

	}
	return Products.findByIdAndUpdate(reqParams.productId,activatedProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return (`You have successfully activated product ID: ${reqParams.productId}`);;
			};
		
		});
	}else{
		return Promise.reject('Not authorized to access this page');
	}
};

// Remove feature

module.exports.unfeature = (data,reqParams,reqBody) =>{

	if(data.isAdmin){
	let deactivateProduct = {
		isFeatured: false

	}
	return Products.findByIdAndUpdate(reqParams.productId,deactivateProduct).then((product,error)=>{
		if(error){
			return false;
		}else{
			return (`You have successfully archived product ID: ${reqParams.productId}`);
		};
	
	});
	}else{
		return Promise.reject('Not authorized to access this page');
	};
};

// 8. GET ALL ORDERS PER PRODUCT ID (ADMIN ONLY)


module.exports.getOrders = (data,reqParams) =>{

	if(data.isAdmin){
	return Products.findById(reqParams.productId).then(result=>{
		return result.orders;
	});
		
	}else{
			return Promise.reject('Not authorized to access this page');
}
};

// 9. ADD PRODUCT FEEDBACK (Any Auth user)

module.exports.productFeedback = async (data,reqParams) =>{

	let productFeedback = await Products.findById(reqParams.productId).then(product=>{	

		product.feedback.push(
			{	
				userId: data.userId,			
				
				comment: data.comment
				
			});		

		return product.save().then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};

		});
		

	});
	if(productFeedback){

	return true;
	}else{
		return false
	}
}

// 10. Retrieve all Product Feedback

module.exports.getfeedback = (reqParams) =>{

	{
	return Products.findById(reqParams.productId).then(result=>{
		return result.feedback;
	});
}		

};


// 10. Retrieve all Product Feedback

module.exports.getCat = (reqParams) =>{

return Products.find({category:reqParams.category,isActive:true}).then(result=>{
		return result
	});
	

};
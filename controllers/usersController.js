const Users = require("../models/Users");
const Products = require("../models/Products");
const Orders = require("../models/Orders");
const bcrypt = require("bcrypt");
const auth = require("../auth");






//1. CHECK IF ACCOUNT EXISTS

module.exports.checkAccountExists = (reqBody) => {
	return Users.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true
		}else{
			return false;
		};
	});
};

// 2. SIGN UP

module.exports.signUpUser = (reqBody)=>{
	let newUsers = new Users({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNumber : reqBody.mobileNumber,
		password : bcrypt.hashSync(reqBody.password,10)

	})

	return newUsers.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

// EDIT USER INFO

module.exports.updateUser = (data,reqBody) =>{

	let updateUser = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNumber: reqBody.mobileNumber,
		address: reqBody.address,
		
		
	}
	return Users.findByIdAndUpdate(data.userId,updateUser).then((user,error)=>{
		if(error){
			return false;
		}else{
			return true
		};
	
		});

	
};

// 3. USER LOG IN

module.exports.verifyUser = (reqBody) =>{
	return Users.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return ("You are not a member yet. Please sign up.");
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false;
			};
		};
	});
};

// 4. RETRIEVE OWN INFO (TOKEN ONLY)

module.exports.getUserInfo = (data) =>{
	return Users.findById(data.userId).then(result=>{result.password = "";
		return result;
});
}


// 5. RETRIEVE ALL USERS INFO (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.getAllUsers = (data)=>{

	console.log(data);

	if(data.isAdmin){

		return Users.find({}).then(result=>{
		return result;	

	});

		
	}else{
		return Promise.reject('Not authorized to access this page');
	};

};

// 6. RETRIEVE SPECIFIC USERS'S INFO USING EMAIL (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.retrieveUserData = (data)=>{

	if(data.isAdmin){

	return Users.find({email:data.email}).then(result=>{
		return result;
		});

	}else{
		return Promise.reject('Not authorized to access this page');
	}
};

// 7. CHANGE USER TO ADMIN (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.activateAdmin = (data,reqParams,reqBody) =>{

	if(data.isAdmin){

		let updateAdmin = {
			isAdmin: true
			}

		return Users.findByIdAndUpdate(reqParams.userId,updateAdmin).then(result=>{
		return result;
	});

	}else{
		return Promise.reject('Not authorized to access this page');
	}	
};

// 8. CHANGE ADMIN TO USER (ADMIN ONLY)
// CHALLENGE: Not showing False Log

module.exports.dectivateAdmin = (data,reqParams,reqBody) =>{

	if(data.isAdmin){

		let updateAdmin = {
			isAdmin: false
			}

		return Users.findByIdAndUpdate(reqParams.userId,updateAdmin).then(result=>{
		return result;
	});
	
	}else{
		return Promise.reject('Not authorized to access this page');
	}	
};



// 9. CREATE ORDER


module.exports.createOrder = async (data) =>{


	let name = await Products.findById(data.productId).then(result=>{
		return result.name;
	});

	let price = await Products.findById(data.productId).then(result=>{
		return result.price;
	});
	
	let total = data.quantity * Number(price);

	let isUserUpdated = await Users.findById(data.userId).then(user=>{

		

		user.orders.push(
			{
				name: name,
				totalAmount: total,
				
				products: [{
					productId:data.productId,
					quantity:data.quantity
									

				}]

			});

		return user;

		// return user.save().then((user,error)=>{
		// 	if(error){
		// 		return false;
		// 	}else{
		// 		return true;
		// 	};
		// });
	});

	let firstName = await Users.findById(data.userId).then(result=>{
		return result.firstName;
	});

	let lastName = await Users.findById(data.userId).then(result=>{
		return result.lastName;
	});

	let address = await Users.findById(data.userId).then(result=>{
		return result.address;
	});



	let isProductUpdated = await Products.findById(data.productId).then(product=>{

		

		product.orders.push(
			{
				firstName: firstName,
				lastName: lastName,
				userId:data.userId,
				quantity:data.quantity,
				
			});

		return product;

		// return product.save().then((product,error)=>{
		// 	if(error){
		// 		return false;
		// 	}else{
		// 		return true;
		// 	};

		// });
		

	});




	if(isUserUpdated&&isProductUpdated){

	let newOrder = Orders ({
		userId:data.userId,
		firstName: firstName,
		lastName: lastName,
		productId: data.productId,
		name: name,
		price: Number(price),
		quantity: data.quantity,
		totalAmount: data.quantity*Number(price)
	}) 
		
		return newOrder.save().then((order,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});

}


}


// 10. RETRIEVE OWN ORDERS (TOKEN ONLY)

module.exports.showCart = (data) =>{
	return Users.findById(data.userId).then(result=>{
		return result.orders;
});
}

// XXXXXXXXXXXXXXXXXXXXXXXXX


//  11. Change Email
module.exports.changePassword = (data,reqBody) =>{



	let updatePassword = {
		password: reqBody.password

		
	}
	return Users.findByIdAndUpdate(data.userId, updatePassword).then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	
		});

	
};



		


const Orders = require("../models/Orders");
const Users = require("../models/Users");
const Products = require("../models/Products");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// 1. CHECK ALL ORDERS (OWN ORDERS)
 

module.exports.allOrders = (data) =>{
	return Orders.find({userId:data.userId}).then(result=>{
		return result;
	});
};




// 2. CHECK A SINGLE ORDER (OWN ORDER)
 

module.exports.getOrder = (data,reqParams) =>{
	return Orders.findById(reqParams.orderId).then(result=>{
		return result;
	});
};


// 3. CHANGE ORDER QUANTITY (OWN ORDER)

module.exports.updateOrder = (reqParams,reqBody) =>{



	let updatedOrder = {
		quantity: reqBody.quantity

		
	}
	return Orders.findByIdAndUpdate(reqParams.orderId,updatedOrder).then((orders,error)=>{
		if(error){
			return false;
		}else{
			return orders;
		};
	
		});

	
};

// 4. REMOVE PRODUCTS FROM CART (OWN ORDER)

module.exports.removeOrder = (data,reqParams) =>{
	return Orders.findByIdAndDelete(reqParams.orderId).then((removed,error)=>{
		if(error){
			return false
		}else{
			return true
		}
	});
};



// 5. CHECK OUT ORDER (OWN ORDER)


module.exports.createOrder = async (data,reqParams) =>{

	let name = await Orders.findById(reqParams.orderId).then(result=>{
		return result.name;
	});

	let price = await Orders.findById(reqParams.orderId).then(result=>{
		return result.price;
	});
	
	let productId = await Orders.findById(reqParams.orderId).then(result=>{
		return result.productId;
	});

	let quantity = await Orders.findById(reqParams.orderId).then(result=>{
		return result.quantity;
	});

	let totalAmount = await Orders.findById(reqParams.orderId).then(result=>{
		return result.totalAmount;
	});
	

	let isUserUpdated = await Users.findById(data.userId).then(user=>{

		

		user.orders.push(
			{
				name: name,
				totalAmount: totalAmount,
				isPaid: true,
				products: [{
					productId:productId,
					quantity:quantity,					
				}]
				

			});

		

		return user.save().then((user,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});



	let isProductUpdated = await Products.findById(productId).then(product=>{

		

		product.orders.push(
			{				
				userId:data.userId,
				quantity: quantity
			});

		

		return product.save().then((product,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};

		});
		

	});




	if(isUserUpdated&&isProductUpdated){

	return Orders.findByIdAndDelete(reqParams.orderId).then((order,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});

}
}

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");



const usersRoute = require("./routes/usersRoute");

const productsRoute = require("./routes/productsRoute");

const ordersRoute = require("./routes/ordersRoute");

const app = express();

mongoose.connect("mongodb+srv://admin123:admin123@project0.sid71ld.mongodb.net/capstone-2-deleon?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);



mongoose.connection.once('open',()=>console.log("Now connected to MongoDB Atlas."))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users",usersRoute);
app.use("/products",productsRoute);
app.use("/orders",ordersRoute);

app.listen(process.env.PORT || 4000,()=>{
	console.log(`API is now online on port ${process.env.PORT||4000}`)
});


const mongoose = require("mongoose");


// PRODUCT SCHEMA

const productSchema = new mongoose.Schema({
		
	
		name: {
			type: String,
			required: [true, "Product Name is required"]
		},
		
		description: {
			type: String,
			required: [true, "Description is required"]
		},

		category: String,

		price: Number,

		photo: String,

		isActive: {
			type: Boolean,
			default: true
		},

		createdOn: {
			type: Date,
			default: new Date()
		},

		
		orders: [
			{
				orderId: String,
				
				userId: String,
					
				quantity: Number,

				purchasedOn: {
					type: Date,
					default: new Date()
				}

			}

		],
				// ONLY AVAILABLE TO AUTH AND VERIFIED BUYER
		feedback: [
			{
				comment:String,
					
				
			
				userId: String,
					
				commentOn: {
					type: Date,
					default: new Date()
				}

			}

		],

		isFeatured: {
			type: Boolean,
			default: false
		}
	



});

module.exports = mongoose.model("Products",productSchema);


const mongoose = require("mongoose");


// ORDERS SCHEMA

const orderSchema = new mongoose.Schema({
		
		
		orderId: String,

		userId: String,

		firstName: String,

		lastName: String,

		address: String,
			

		productId: String,
		
		name: String,
					
		price: Number,

		quantity: Number,
		
		totalAmount: Number,

		isPaid: {
			type: Boolean,
			default: false
		},

		addedOn: {
			type: Date,
			default: new Date(),
				},
		




});

module.exports = mongoose.model("Orders",orderSchema);
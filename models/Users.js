

const mongoose = require("mongoose");

// USER SCHEMA

const userSchema = new mongoose.Schema({
	
	firstName: {
			type: String,
			required: [true, "First Name is required"]
		},

	lastName: {
			type: String,
			required: [true, "Last Name is required"]
		},

	email: {
			type: String,
			required: [true, "Email Address is required"]
		},


	password: {
			type: String,
			required: [true, "Password is required"]
		},

	isAdmin: {
			type: Boolean,
			default: false
		},

	mobileNumber: {
			type: String,
			required: [true, "Mobile Number is required"]
		},
	
	orders: [
		
		{
			

			name: String,

			totalAmount: Number,

			purchasedOn: {
					type: Date,
					     default: new Date(),
					 	},

			products: [

					{
						productId: String,
						quantity: Number
						
					}

					  ],
			isPaid: {
						type: Boolean,
						default: false
					},

		}

	],

	address: String,
			


});

module.exports = mongoose.model("User",userSchema);